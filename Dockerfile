FROM alpine:3.4

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apk --no-cache --update add nodejs
RUN npm install -g reveal-md \
    && mkdir -p /usr/lib/node_modules/reveal-md/node_modules/reveal.js/images

COPY resources/css/zaaksysteem.css /usr/lib/node_modules/reveal-md/node_modules/reveal.js/css/theme/zaaksysteem.css
COPY resources/css/source/zaaksysteem.scss /usr/lib/node_modules/reveal-md/node_modules/reveal.js/css/theme/source/zaaksysteem.scss

COPY resources/reveal.html /usr/lib/node_modules/reveal-md/lib/template/reveal.html
COPY resources/listing.html /usr/lib/node_modules/reveal-md/lib/template/listing.html

EXPOSE 8182
ENTRYPOINT ["reveal-md", "--theme", "zaaksysteem", "--port", "8182"]
