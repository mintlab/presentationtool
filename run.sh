docker stop presentationtool
docker run --rm --name presentationtool -d -v $PWD/../presentations:/usr/src/app -p 8182:8182 registry.gitlab.com/mintlab/presentationtool
