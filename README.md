# SSL Gateway for SSL Offloading

## BUILD

```
docker build -t registry.gitlab.com/mintlab/presentationtool .
```

## Run with current directory as source for presentations

```
docker run -v $PWD:/usr/src/app -p 8182:8182 registry.gitlab.com/mintlab/presentationtool
```
