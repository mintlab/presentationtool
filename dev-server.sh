docker kill presentationtool
docker run --rm --name presentationtool -d -v $PWD/../presentations:/usr/src/app -v $PWD/resources/css/zaaksysteem.css:/usr/lib/node_modules/reveal-md/node_modules/reveal.js/css/theme/zaaksysteem.css -v $PWD/resources/reveal.html:/usr/lib/node_modules/reveal-md/lib/template/reveal.html -v $PWD/resources/listing.html:/usr/lib/node_modules/reveal-md/lib/template/listing.html -p 8182:8182 registry.gitlab.com/mintlab/presentationtool
node-sass -o resources/css resources/css/source/zaaksysteem.scss
node-sass -o resources/css --watch resources/css/source/zaaksysteem.scss
